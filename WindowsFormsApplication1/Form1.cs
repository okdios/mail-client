﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public static Form1 staticVar = null;
        public Form1(int selectedMail, int action)
        {
            InitializeComponent();

            // action:
            // 0 = new mail
            // 1 = reply
            // 2 = forward
            // 3 = send public key
            string selectedSender = "unknown";
            string selectedSubject = "unknown";
            string selectedBody = "unknown";
            if (action == 1 || action == 2)
            {
                SQLiteDatabase db = new SQLiteDatabase();
                DataTable Mail;
                String query = "select Sender \"Sender\", Subject \"Subject\",";
                query += "Body \"Body\", Timestamp \"Timestamp\"";
                query += "from Mails ";
                query += "where ID = " + selectedMail + ";";
                Mail = db.GetDataTable(query);
                foreach (DataRow r in Mail.Rows)
                {
                    selectedSender = r["Sender"].ToString();
                    selectedSubject = r["Subject"].ToString();
                    selectedBody = r["Body"].ToString();
                }
            }

            switch (action)
            {
                case 1: // Reply
                    textBoxTo.Text = selectedSender;
                    textBoxSub.Text = "RE: " + selectedSubject;
                    break;
                case 2: // Forward
                    textBoxTo.Text = "";
                    textBoxSub.Text = "FW: " + selectedSubject;
                    break;
                case 3:
                    textBoxSub.Text = "My public key";
                    textBoxBody.Text = Properties.Settings.Default.RSAPublic;
                    textBoxPublicKey.Visible = false;
                    label1.Visible = false;
                    checkBoxEncrypt.Visible = false;
                    checkBoxRSA.Visible = false;
                    break;
                default:
                    textBoxTo.Text = "";
                    textBoxSub.Text = "";
                    break;
            }
        }

        private void buttonsend_Click(object sender, EventArgs e)
        {   // send button
            SmtpClient client = new SmtpClient(Properties.Settings.Default.SmtpServer, Properties.Settings.Default.SmtpPort);
            client.Credentials = new NetworkCredential(Properties.Settings.Default.SmtpUsername, Properties.Settings.Default.SmtpPassword);
            MailMessage msg = new MailMessage();
            
            // Check if it is set!
            msg.From = new MailAddress(Properties.Settings.Default.SmtpUsername);
            msg.To.Add(new MailAddress(textBoxTo.Text));
            msg.Subject = textBoxSub.Text;

            string bodyText = textBoxBody.Text;

            if (checkBoxEncrypt.Checked == true)
            {
                try
                {
                    // Encrypt
                    bodyText = RijndaelSimple.Encrypt(textBoxBody.Text,                                             // Text to encrypt
                                                                  Properties.Settings.Default.EncryptionPassphrase, // Any string
                                                                  "MySaltValue234",                                 // Any string
                                                                  "SHA1",                                           // SHA1 or MD5
                                                                  2,                                                // Any number
                                                                  "p6mdJ84b#siO1JsS",                               // Must be 16 bytes
                                                                  256);                                             // 128, 192 or 256
                }
                catch (Exception encException)
                {
                    MessageBox.Show("Something went wrong with the encryption! Error: " + encException);
                }
            }

            else if (checkBoxRSA.Checked == true)
            {
                UnicodeEncoding byteConverted = new UnicodeEncoding();

                byte[] dataToEncrypt = byteConverted.GetBytes(textBoxBody.Text);

                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    RSA.FromXmlString(textBoxPublicKey.Text);
                    byte[] encryptedData = RSA.Encrypt(dataToEncrypt, false);
                    string data = Convert.ToBase64String(encryptedData);
                    bodyText = data;
                }
            }

            msg.Body = bodyText;

            client.EnableSsl = true;
            try
            {
                client.Send(msg);
            }
            catch(Exception ex)
            {
                MessageBox.Show("blabla: " + ex.Message);
            }

            staticVar = this;
            this.Hide();
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {   // auto fill button
            textBoxTo.Text = Properties.Settings.Default.PopUsername;
            //textBoxFrom.Text = Properties.Settings.Default.SmtpUsername;
            textBoxSub.Text = "det virker...";
            textBoxBody.Text = "dette er en test";
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {   // cancel button
            this.Close();
        }
    }
}
