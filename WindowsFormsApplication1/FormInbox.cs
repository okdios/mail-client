﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Data.SQLite;
using System.Security.Cryptography;
using OpenPop.Mime;
using OpenPop.Mime.Header;
using OpenPop.Pop3;
using OpenPop.Pop3.Exceptions;
using OpenPop.Common.Logging;
using Message = OpenPop.Mime.Message;


namespace WindowsFormsApplication1
{
    public partial class FormInbox : Form
    {
        int selectedMail = 0;

        private ListViewColumnSorter lvwColumnSorter;

        public static FormInbox staticVar = null;
        private Pop3Client pop3client;
        private Dictionary<int, Message> messages;
        public FormInbox()
        {
            InitializeComponent();

            // generate RSA keys
            //MessageBox.Show(Properties.Settings.Default.RSAPrivate);
            GenerateKeys();
            //MessageBox.Show(Properties.Settings.Default.RSAPrivate);
            //MessageBox.Show(Properties.Settings.Default.RSAPublic);
            
            backgroundWorker1.RunWorkerAsync();
            listMails();
            pop3client = new Pop3Client();
            messages = new Dictionary<int,Message>();

            // Create an instance of a ListView column sorter and assign it 
            // to the ListView control.
            lvwColumnSorter = new ListViewColumnSorter();
            this.listViewInbox.ListViewItemSorter = lvwColumnSorter;
        }

        private void buttonReceive_Click(object sender, EventArgs e)
        {
            ReceiveMails();
        }

        private void ReceiveMails()
        {
            try
            {
                if (pop3client.Connected)
                    pop3client.Disconnect();
                pop3client.Connect(Properties.Settings.Default.PopServer, Properties.Settings.Default.PopPort, Properties.Settings.Default.PopSsl);
                pop3client.Authenticate(Properties.Settings.Default.PopUsername, Properties.Settings.Default.PopPassword);
                int count = pop3client.GetMessageCount();
                messages.Clear();
                int success = 0;
                int fail = 0;
                for (int i = count; i >= 1; i -= 1)
                {
                    try
                    {
                        string body;
                        Message message = pop3client.GetMessage(i);
                        MessagePart plainTextPart = message.FindFirstPlainTextVersion();

                        MessageHeader headers = pop3client.GetMessageHeaders(i);
                        RfcMailAddress from = headers.From;

                        string subject = headers.Subject;
                        DateTime Time = headers.DateSent.AddHours(2);
                        string messageID = headers.MessageId;

                        // Check if sender is you (returns -1 if there is no match)
                        int checkPos = from.ToString().ToLower().IndexOf(Properties.Settings.Default.PopUsername.ToLower());

                        if (plainTextPart != null && checkPos < 0)
                        {
                            body = plainTextPart.GetBodyAsText();
                            bool mailExist = mailExistFunction(messageID);
                            if (mailExist != true)
                                InsertMails(from.ToString(), subject.ToString(), body.ToString(), Time.ToString(), messageID);
                        }
                        else
                        {
                            List<MessagePart> textVersions = message.FindAllTextVersions();
                            if (textVersions.Count >= 1)
                                body = textVersions[0].GetBodyAsText();
                            else
                                body = "<<OpenPop>> error <<OpenPop>>";
                        }
                        List<MessagePart> attachments = message.FindAllAttachments();
                        foreach (MessagePart attachment in attachments)
                        { }
                        messages.Add(i, message);
                        success++;

                        // Print progres, and update progressbar
                        int progressPercentage = Convert.ToInt32(((success+fail)*100)/count);
                        backgroundWorker1.ReportProgress(progressPercentage);
                    }
                    catch (Exception e)
                    {
                        fail++;
                        MessageBox.Show(e.ToString());
                    }
                }

                //////////////////////////////
                sortTime();
                //////////////////////////////
            }
            catch (InvalidLoginException)
            {
            }
            catch (PopServerNotFoundException)
            {
            }
            catch (PopServerLockedException)
            { }
            catch (LoginDelayException)
            { }
            catch (Exception e)
            { }
            finally
            { }
        }

        private void GetMails()
        {
            try
            {
                SQLiteDatabase db = new SQLiteDatabase();
                DataTable Mails;
                String query = "select Sender \"Sender\", Subject \"Subject\",";
                query += "Body \"Body\", TimeReceived \"TimeReceived\"";
                query += "from Mails;";
                Mails = db.GetDataTable(query);

                foreach (DataRow r in Mails.Rows)
                {
                    //MessageBox.Show(r["Sender"].ToString());
                    //MessageBox.Show(r["Subject"].ToString());
                    //MessageBox.Show(r["Body"].ToString());
                    //MessageBox.Show(r["TimeReceived"].ToString());
                }
            }
            catch (Exception fail)
            {
                String error = "The following error has occurred:\n\n";
                error += fail.Message.ToString() + "\n\n";
                MessageBox.Show(error);
                this.Close();
            }
        }

        private bool mailExistFunction(string newMessageID)
        {
            SQLiteDatabase db = new SQLiteDatabase();
            DataTable Mails;

            String query = "select ID \"ID\" from Mails where gmailID = '" + newMessageID + "';";

            Mails = db.GetDataTable(query);
            if (Mails == null || Mails.Rows.Count == 0)
            {
                return false;
            }

            return true;
        }


        private void InsertMails(string newSender, string newSubject, string newBody, string newDatetime, string newMessageID)
        {
            SQLiteDatabase db = new SQLiteDatabase();
            Dictionary<String, String> data = new Dictionary<String, String>();
            data.Add("Sender", newSender);
            data.Add("Subject", newSubject);
            data.Add("Body", newBody);
            data.Add("Timestamp", newDatetime);
            data.Add("gmailID", newMessageID);
            
            try
            {
                db.Insert("Mails", data);
            }
            catch (Exception e)
            {
                //MessageBox.Show(crap.Message);
                String error = "The following error has occurred:\n\n";
                error += e.Message.ToString() + "\n\n";
                MessageBox.Show(error);
            }
        }

        private void listMails()
        {
            try
            {
                listViewInbox.Items.Clear();
                SQLiteDatabase db = new SQLiteDatabase();
                DataTable Mails;
                
                String query = "select ID \"ID\", Sender \"Sender\", Subject \"Subject\", Timestamp \"Timestamp\"";
                query += "from Mails;";
                
                Mails = db.GetDataTable(query);

                foreach (DataRow r in Mails.Rows)
                {
                    ListViewItem newItem = new ListViewItem(r["ID"].ToString());
                    newItem.SubItems.Add(r["Subject"].ToString());
                    newItem.SubItems.Add(r["Sender"].ToString());
                    newItem.SubItems.Add(r["Timestamp"].ToString());
                    listViewInbox.Items.Add(newItem);
                }
            }
            catch (Exception fail)
            {
                String error = "The following error has occurred:\n\n";
                error += fail.Message.ToString() + "\n\n";
                MessageBox.Show(error);
                this.Close();
            }
        }

        private void buttonView_Click(object sender, EventArgs e)
        {
            GetMails();
        }

        private void buttonListMails_Click(object sender, EventArgs e)
        {   // Refresh button
            if (backgroundWorker1.IsBusy != true)
                backgroundWorker1.RunWorkerAsync();
            //ReceiveMails();
            listMails();
        }

        private void listViewInbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            string selected = lv.FocusedItem.SubItems[0].Text.ToString();
            listMails();

            getEmailFromID(Convert.ToInt32(selected), false, false);
            selectedMail = Convert.ToInt32(selected);
        }

        private void getEmailFromID(int mailID, bool TypeIsRsa, bool decrypt)
        {
            try
            {
                SQLiteDatabase db = new SQLiteDatabase();
                DataTable Mails;
                String query = "select Sender \"Sender\", Subject \"Subject\",";
                query += "Body \"Body\", Timestamp \"Timestamp\"";
                query += "from Mails ";
                query += "where ID = " + mailID + ";";
                Mails = db.GetDataTable(query);
                
                foreach (DataRow r in Mails.Rows)
                {
                    textBoxFrom.Text = r["Sender"].ToString();
                    textBoxSubject.Text = r["Subject"].ToString();

                    string bodyText = r["Body"].ToString();

                    if (TypeIsRsa == false && decrypt == true)
                    {
                        try
                        {
                            // Decrypt
                            bodyText = RijndaelSimple.Decrypt(r["Body"].ToString(),   // Text to decrypt
                                                                textBoxDecrypt.Text,  // Any string
                                                                "MySaltValue234",     // Any string
                                                                "SHA1",               // SHA1 or MD5
                                                                2,                    // Any number
                                                                "p6mdJ84b#siO1JsS",   // Must be 16 bytes
                                                                256);                 // 128, 192 or 256
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Incorrect password!");
                        }
                    }
                    else if (TypeIsRsa == true && decrypt == true)
                    {
                        UnicodeEncoding byteConverted = new UnicodeEncoding();
                        byte[] encryptedData = Convert.FromBase64String(r["Body"].ToString());

                        using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                        {
                            RSA.FromXmlString(Properties.Settings.Default.RSAPrivate);
                            byte[] decryptedData = RSA.Decrypt(encryptedData, false);
                            bodyText = byteConverted.GetString(decryptedData);
                        }
                    }

                    else
                        bodyText = r["Body"].ToString();

                    textBoxBody.Text = bodyText;
                }
            }
            catch (Exception fail)
            {
                String error = "The following error has occurred:\n\n";
                error += fail.Message.ToString() + "\n\n";
                MessageBox.Show(error);
                this.Close();
            }
        }

        private void deleteFromDB(int mailID)
        {
            MessageBox.Show("message deleted. ID: " + mailID);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ReceiveMails();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarMail.Visible = true;
            progressBarMail.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            listMails();
            progressBarMail.Visible = false;
        }

        private void buttonReply_Click(object sender, EventArgs e)
        {   // Mail - Reply
            staticVar = this;
            Form1 form1 = new Form1(selectedMail, 1);
            form1.Show();
        }

        private void buttonForward_Click(object sender, EventArgs e)
        {   // Mail - Forward
            staticVar = this;
            Form1 form1 = new Form1(selectedMail, 2);
            form1.Show();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {   // Mail - Delete
            SQLiteDatabase db = new SQLiteDatabase();
            DataTable Mails;
            String query = "delete from Mails where ID = " + selectedMail + ";";
            Mails = db.GetDataTable(query);
            
            // Delete from the listView...

            listMails();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {   // Menu - New mail
            staticVar = this;
            //this.Hide();
            Form1 form1 = new Form1(selectedMail, 0);
            form1.Show();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {   // Menu - Refresh
            if (backgroundWorker1.IsBusy != true)
                backgroundWorker1.RunWorkerAsync();
        }

        private void settingsToolStripMenuItem1_Click(object sender, EventArgs e)
        {   // Menu - Settings
            staticVar = this;
            FormSettings formSettings = new FormSettings();
            formSettings.Show();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {   // Menu - Quit
            this.Close();
        }

        private void checkBoxDecrypt_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxDecrypt.Checked == true)
                getEmailFromID(selectedMail, false, true);
            else
                getEmailFromID(selectedMail, false, false);
        }

        private void checkBoxRSADecrypt_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxRSADecrypt.Checked == true)
                getEmailFromID(selectedMail, true, true);
            else
                getEmailFromID(selectedMail, true, false);
        }

        private void GenerateKeys()
        {
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
            string keyPrivate = csp.ToXmlString(true);
            string keyPublic = csp.ToXmlString(false);

            if (Properties.Settings.Default.RSAPrivate == "" || Properties.Settings.Default.RSAPrivate == null)
                Properties.Settings.Default.RSAPrivate = keyPrivate;
            if (Properties.Settings.Default.RSAPublic == "" || Properties.Settings.Default.RSAPublic == null)
                Properties.Settings.Default.RSAPublic = keyPublic;

            Properties.Settings.Default.Save();
        }

        private void sendPublicKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            staticVar = this;
            Form1 form1 = new Form1(0, 3);
            form1.Show();
        }

        private void listViewInbox_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            //MessageBox.Show(e.Column.ToString());
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.listViewInbox.Sort();
        }

        private void sortTime()
        {
            //lvwColumnSorter.SortColumn = listViewInbox.
            //lvwColumnSorter.Order = SortOrder.Descending;
            //this.listViewInbox.Sort();
        }


    }


    public class ListViewColumnSorter : IComparer
    {
        /// <summary>
        /// Specifies the column to be sorted
        /// </summary>
        private int ColumnToSort;
        /// <summary>
        /// Specifies the order in which to sort (i.e. 'Ascending').
        /// </summary>
        private SortOrder OrderOfSort;
        /// <summary>
        /// Case insensitive comparer object
        /// </summary>
        private CaseInsensitiveComparer ObjectCompare;

        /// <summary>
        /// Class constructor.  Initializes various elements
        /// </summary>
        public ListViewColumnSorter()
        {
            // Initialize the column to '0'
            ColumnToSort = 0;

            // Initialize the sort order to 'none'
            OrderOfSort = SortOrder.None;

            // Initialize the CaseInsensitiveComparer object
            ObjectCompare = new CaseInsensitiveComparer();
        }

        /// <summary>
        /// This method is inherited from the IComparer interface.  It compares the two objects passed using a case insensitive comparison.
        /// </summary>
        /// <param name="x">First object to be compared</param>
        /// <param name="y">Second object to be compared</param>
        /// <returns>The result of the comparison. "0" if equal, negative if 'x' is less than 'y' and positive if 'x' is greater than 'y'</returns>
        public int Compare(object x, object y)
        {
            int compareResult;
            ListViewItem listviewX, listviewY;

            // Cast the objects to be compared to ListViewItem objects
            listviewX = (ListViewItem)x;
            listviewY = (ListViewItem)y;

            // Compare the two items
            compareResult = ObjectCompare.Compare(listviewX.SubItems[ColumnToSort].Text, listviewY.SubItems[ColumnToSort].Text);

            // Calculate correct return value based on object comparison
            if (OrderOfSort == SortOrder.Ascending)
            {
                // Ascending sort is selected, return normal result of compare operation
                return compareResult;
            }
            else if (OrderOfSort == SortOrder.Descending)
            {
                // Descending sort is selected, return negative result of compare operation
                return (-compareResult);
            }
            else
            {
                // Return '0' to indicate they are equal
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>
        public int SortColumn
        {
            set
            {
                ColumnToSort = value;
            }
            get
            {
                return ColumnToSort;
            }
        }

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>
        public SortOrder Order
        {
            set
            {
                OrderOfSort = value;
            }
            get
            {
                return OrderOfSort;
            }
        }

    }

}
