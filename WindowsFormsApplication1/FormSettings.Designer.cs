﻿namespace WindowsFormsApplication1
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSmtpServer = new System.Windows.Forms.TextBox();
            this.textBoxSmtpPort = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxSmtpPassword2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxSmtpSsl = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSmtpPassword1 = new System.Windows.Forms.TextBox();
            this.textBoxSmtpUsername = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxPopPassword2 = new System.Windows.Forms.TextBox();
            this.checkBoxPopSsl = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxPopPassword1 = new System.Windows.Forms.TextBox();
            this.textBoxPopUsername = new System.Windows.Forms.TextBox();
            this.textBoxPopPort = new System.Windows.Forms.TextBox();
            this.textBoxPopServer = new System.Windows.Forms.TextBox();
            this.buttonSettingsSave = new System.Windows.Forms.Button();
            this.buttonSettingsCencel = new System.Windows.Forms.Button();
            this.checkBoxBlue = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxPassphrase2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxPassphrase1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxSmtpServer
            // 
            this.textBoxSmtpServer.Location = new System.Drawing.Point(149, 19);
            this.textBoxSmtpServer.Name = "textBoxSmtpServer";
            this.textBoxSmtpServer.Size = new System.Drawing.Size(256, 20);
            this.textBoxSmtpServer.TabIndex = 0;
            this.textBoxSmtpServer.TextChanged += new System.EventHandler(this.textBoxSmtpServer_TextChanged);
            // 
            // textBoxSmtpPort
            // 
            this.textBoxSmtpPort.Location = new System.Drawing.Point(149, 45);
            this.textBoxSmtpPort.Name = "textBoxSmtpPort";
            this.textBoxSmtpPort.Size = new System.Drawing.Size(256, 20);
            this.textBoxSmtpPort.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxSmtpPassword2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.checkBoxSmtpSsl);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxSmtpPassword1);
            this.groupBox1.Controls.Add(this.textBoxSmtpUsername);
            this.groupBox1.Controls.Add(this.textBoxSmtpServer);
            this.groupBox1.Controls.Add(this.textBoxSmtpPort);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(411, 172);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SMTP";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Retype password";
            // 
            // textBoxSmtpPassword2
            // 
            this.textBoxSmtpPassword2.Location = new System.Drawing.Point(149, 124);
            this.textBoxSmtpPassword2.Name = "textBoxSmtpPassword2";
            this.textBoxSmtpPassword2.PasswordChar = '*';
            this.textBoxSmtpPassword2.Size = new System.Drawing.Size(256, 20);
            this.textBoxSmtpPassword2.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Use SSL?";
            // 
            // checkBoxSmtpSsl
            // 
            this.checkBoxSmtpSsl.AutoSize = true;
            this.checkBoxSmtpSsl.Location = new System.Drawing.Point(149, 150);
            this.checkBoxSmtpSsl.Name = "checkBoxSmtpSsl";
            this.checkBoxSmtpSsl.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSmtpSsl.TabIndex = 5;
            this.checkBoxSmtpSsl.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Server";
            // 
            // textBoxSmtpPassword1
            // 
            this.textBoxSmtpPassword1.Location = new System.Drawing.Point(149, 97);
            this.textBoxSmtpPassword1.Name = "textBoxSmtpPassword1";
            this.textBoxSmtpPassword1.PasswordChar = '*';
            this.textBoxSmtpPassword1.Size = new System.Drawing.Size(256, 20);
            this.textBoxSmtpPassword1.TabIndex = 3;
            // 
            // textBoxSmtpUsername
            // 
            this.textBoxSmtpUsername.Location = new System.Drawing.Point(149, 71);
            this.textBoxSmtpUsername.Name = "textBoxSmtpUsername";
            this.textBoxSmtpUsername.Size = new System.Drawing.Size(256, 20);
            this.textBoxSmtpUsername.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.textBoxPopPassword2);
            this.groupBox2.Controls.Add(this.checkBoxPopSsl);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBoxPopPassword1);
            this.groupBox2.Controls.Add(this.textBoxPopUsername);
            this.groupBox2.Controls.Add(this.textBoxPopPort);
            this.groupBox2.Controls.Add(this.textBoxPopServer);
            this.groupBox2.Location = new System.Drawing.Point(13, 192);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(411, 173);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "POP";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Retype password";
            // 
            // textBoxPopPassword2
            // 
            this.textBoxPopPassword2.Location = new System.Drawing.Point(150, 123);
            this.textBoxPopPassword2.Name = "textBoxPopPassword2";
            this.textBoxPopPassword2.PasswordChar = '*';
            this.textBoxPopPassword2.Size = new System.Drawing.Size(256, 20);
            this.textBoxPopPassword2.TabIndex = 10;
            // 
            // checkBoxPopSsl
            // 
            this.checkBoxPopSsl.AutoSize = true;
            this.checkBoxPopSsl.Location = new System.Drawing.Point(150, 149);
            this.checkBoxPopSsl.Name = "checkBoxPopSsl";
            this.checkBoxPopSsl.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPopSsl.TabIndex = 11;
            this.checkBoxPopSsl.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 149);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Use SSL?";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Password";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Username";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Port";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Server";
            // 
            // textBoxPopPassword1
            // 
            this.textBoxPopPassword1.Location = new System.Drawing.Point(149, 97);
            this.textBoxPopPassword1.Name = "textBoxPopPassword1";
            this.textBoxPopPassword1.PasswordChar = '*';
            this.textBoxPopPassword1.Size = new System.Drawing.Size(256, 20);
            this.textBoxPopPassword1.TabIndex = 9;
            // 
            // textBoxPopUsername
            // 
            this.textBoxPopUsername.Location = new System.Drawing.Point(149, 71);
            this.textBoxPopUsername.Name = "textBoxPopUsername";
            this.textBoxPopUsername.Size = new System.Drawing.Size(256, 20);
            this.textBoxPopUsername.TabIndex = 8;
            // 
            // textBoxPopPort
            // 
            this.textBoxPopPort.Location = new System.Drawing.Point(149, 45);
            this.textBoxPopPort.Name = "textBoxPopPort";
            this.textBoxPopPort.Size = new System.Drawing.Size(256, 20);
            this.textBoxPopPort.TabIndex = 7;
            // 
            // textBoxPopServer
            // 
            this.textBoxPopServer.Location = new System.Drawing.Point(149, 19);
            this.textBoxPopServer.Name = "textBoxPopServer";
            this.textBoxPopServer.Size = new System.Drawing.Size(256, 20);
            this.textBoxPopServer.TabIndex = 6;
            // 
            // buttonSettingsSave
            // 
            this.buttonSettingsSave.Location = new System.Drawing.Point(316, 444);
            this.buttonSettingsSave.Name = "buttonSettingsSave";
            this.buttonSettingsSave.Size = new System.Drawing.Size(108, 27);
            this.buttonSettingsSave.TabIndex = 12;
            this.buttonSettingsSave.Text = "Save";
            this.buttonSettingsSave.UseVisualStyleBackColor = true;
            this.buttonSettingsSave.Click += new System.EventHandler(this.buttonSettingsSave_Click);
            // 
            // buttonSettingsCencel
            // 
            this.buttonSettingsCencel.Location = new System.Drawing.Point(202, 444);
            this.buttonSettingsCencel.Name = "buttonSettingsCencel";
            this.buttonSettingsCencel.Size = new System.Drawing.Size(108, 27);
            this.buttonSettingsCencel.TabIndex = 13;
            this.buttonSettingsCencel.Text = "Cancel";
            this.buttonSettingsCencel.UseVisualStyleBackColor = true;
            this.buttonSettingsCencel.Click += new System.EventHandler(this.buttonSettingsCencel_Click);
            // 
            // checkBoxBlue
            // 
            this.checkBoxBlue.AutoSize = true;
            this.checkBoxBlue.Location = new System.Drawing.Point(13, 450);
            this.checkBoxBlue.Name = "checkBoxBlue";
            this.checkBoxBlue.Size = new System.Drawing.Size(91, 17);
            this.checkBoxBlue.TabIndex = 14;
            this.checkBoxBlue.Text = "Blue buttons?";
            this.checkBoxBlue.UseVisualStyleBackColor = true;
            this.checkBoxBlue.CheckedChanged += new System.EventHandler(this.checkBoxBlue_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBoxPassphrase2);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textBoxPassphrase1);
            this.groupBox3.Location = new System.Drawing.Point(13, 370);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(411, 68);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Encryption";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 43);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Retype passphrase";
            // 
            // textBoxPassphrase2
            // 
            this.textBoxPassphrase2.Location = new System.Drawing.Point(149, 41);
            this.textBoxPassphrase2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPassphrase2.Name = "textBoxPassphrase2";
            this.textBoxPassphrase2.PasswordChar = '*';
            this.textBoxPassphrase2.Size = new System.Drawing.Size(256, 20);
            this.textBoxPassphrase2.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 20);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Passphrase";
            // 
            // textBoxPassphrase1
            // 
            this.textBoxPassphrase1.Location = new System.Drawing.Point(150, 17);
            this.textBoxPassphrase1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPassphrase1.Name = "textBoxPassphrase1";
            this.textBoxPassphrase1.PasswordChar = '*';
            this.textBoxPassphrase1.Size = new System.Drawing.Size(255, 20);
            this.textBoxPassphrase1.TabIndex = 0;
            // 
            // FormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 479);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.checkBoxBlue);
            this.Controls.Add(this.buttonSettingsCencel);
            this.Controls.Add(this.buttonSettingsSave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormSettings";
            this.Text = "MO-Mail - Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSmtpServer;
        private System.Windows.Forms.TextBox textBoxSmtpPort;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxSmtpSsl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSmtpPassword1;
        private System.Windows.Forms.TextBox textBoxSmtpUsername;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxPopPassword1;
        private System.Windows.Forms.TextBox textBoxPopUsername;
        private System.Windows.Forms.TextBox textBoxPopPort;
        private System.Windows.Forms.TextBox textBoxPopServer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxSmtpPassword2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxPopPassword2;
        private System.Windows.Forms.CheckBox checkBoxPopSsl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonSettingsSave;
        private System.Windows.Forms.Button buttonSettingsCencel;
        private System.Windows.Forms.CheckBox checkBoxBlue;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxPassphrase2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxPassphrase1;
    }
}