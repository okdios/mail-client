﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonsend = new System.Windows.Forms.Button();
            this.textto = new System.Windows.Forms.Label();
            this.textsub = new System.Windows.Forms.Label();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.textBoxSub = new System.Windows.Forms.TextBox();
            this.textBoxBody = new System.Windows.Forms.TextBox();
            this.buttonTest = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.checkBoxEncrypt = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxRSA = new System.Windows.Forms.CheckBox();
            this.textBoxPublicKey = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonsend
            // 
            this.buttonsend.Location = new System.Drawing.Point(458, 11);
            this.buttonsend.Name = "buttonsend";
            this.buttonsend.Size = new System.Drawing.Size(75, 71);
            this.buttonsend.TabIndex = 0;
            this.buttonsend.Text = "Send";
            this.buttonsend.UseVisualStyleBackColor = true;
            this.buttonsend.Click += new System.EventHandler(this.buttonsend_Click);
            // 
            // textto
            // 
            this.textto.AutoSize = true;
            this.textto.Location = new System.Drawing.Point(8, 13);
            this.textto.Name = "textto";
            this.textto.Size = new System.Drawing.Size(23, 13);
            this.textto.TabIndex = 1;
            this.textto.Text = "To:";
            // 
            // textsub
            // 
            this.textsub.AutoSize = true;
            this.textsub.Location = new System.Drawing.Point(8, 40);
            this.textsub.Name = "textsub";
            this.textsub.Size = new System.Drawing.Size(46, 13);
            this.textsub.TabIndex = 3;
            this.textsub.Text = "Subject:";
            // 
            // textBoxTo
            // 
            this.textBoxTo.Location = new System.Drawing.Point(88, 11);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(366, 20);
            this.textBoxTo.TabIndex = 5;
            // 
            // textBoxSub
            // 
            this.textBoxSub.Location = new System.Drawing.Point(88, 37);
            this.textBoxSub.Name = "textBoxSub";
            this.textBoxSub.Size = new System.Drawing.Size(366, 20);
            this.textBoxSub.TabIndex = 7;
            // 
            // textBoxBody
            // 
            this.textBoxBody.Location = new System.Drawing.Point(10, 89);
            this.textBoxBody.Multiline = true;
            this.textBoxBody.Name = "textBoxBody";
            this.textBoxBody.Size = new System.Drawing.Size(524, 339);
            this.textBoxBody.TabIndex = 8;
            // 
            // buttonTest
            // 
            this.buttonTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTest.Location = new System.Drawing.Point(37, 11);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(33, 18);
            this.buttonTest.TabIndex = 9;
            this.buttonTest.Text = "auto";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(377, 60);
            this.buttonBack.Margin = new System.Windows.Forms.Padding(2);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 20);
            this.buttonBack.TabIndex = 10;
            this.buttonBack.Text = "Cancel";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // checkBoxEncrypt
            // 
            this.checkBoxEncrypt.AutoSize = true;
            this.checkBoxEncrypt.Location = new System.Drawing.Point(88, 64);
            this.checkBoxEncrypt.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxEncrypt.Name = "checkBoxEncrypt";
            this.checkBoxEncrypt.Size = new System.Drawing.Size(15, 14);
            this.checkBoxEncrypt.TabIndex = 11;
            this.checkBoxEncrypt.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 63);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Encrypt:";
            // 
            // checkBoxRSA
            // 
            this.checkBoxRSA.AutoSize = true;
            this.checkBoxRSA.Location = new System.Drawing.Point(123, 62);
            this.checkBoxRSA.Name = "checkBoxRSA";
            this.checkBoxRSA.Size = new System.Drawing.Size(48, 17);
            this.checkBoxRSA.TabIndex = 13;
            this.checkBoxRSA.Text = "RSA";
            this.checkBoxRSA.UseVisualStyleBackColor = true;
            // 
            // textBoxPublicKey
            // 
            this.textBoxPublicKey.Location = new System.Drawing.Point(177, 61);
            this.textBoxPublicKey.Name = "textBoxPublicKey";
            this.textBoxPublicKey.Size = new System.Drawing.Size(100, 20);
            this.textBoxPublicKey.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 436);
            this.Controls.Add(this.textBoxPublicKey);
            this.Controls.Add(this.checkBoxRSA);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxEncrypt);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.textBoxBody);
            this.Controls.Add(this.textBoxSub);
            this.Controls.Add(this.textBoxTo);
            this.Controls.Add(this.textsub);
            this.Controls.Add(this.textto);
            this.Controls.Add(this.buttonsend);
            this.Name = "Form1";
            this.Text = "MO-Mail - Send";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonsend;
        private System.Windows.Forms.Label textto;
        private System.Windows.Forms.Label textsub;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.TextBox textBoxSub;
        private System.Windows.Forms.TextBox textBoxBody;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.CheckBox checkBoxEncrypt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxRSA;
        private System.Windows.Forms.TextBox textBoxPublicKey;
    }
}

