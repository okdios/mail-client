﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
            
            // SMTP settings
            textBoxSmtpServer.Text = Properties.Settings.Default.SmtpServer;
            textBoxSmtpPort.Text = Properties.Settings.Default.SmtpPort.ToString();
            textBoxSmtpUsername.Text = Properties.Settings.Default.SmtpUsername;
            textBoxSmtpPassword1.Text = Properties.Settings.Default.SmtpPassword;
            textBoxSmtpPassword2.Text = Properties.Settings.Default.SmtpPassword;
            if (Properties.Settings.Default.SmtpSsl == true)
                checkBoxSmtpSsl.Checked = true;
            else
                checkBoxSmtpSsl.Checked = false;

            // POP settings
            textBoxPopServer.Text = Properties.Settings.Default.PopServer;
            textBoxPopPort.Text = Properties.Settings.Default.PopPort.ToString();
            textBoxPopUsername.Text = Properties.Settings.Default.PopUsername;
            textBoxPopPassword1.Text = Properties.Settings.Default.PopPassword;
            textBoxPopPassword2.Text = Properties.Settings.Default.PopPassword;
            if (Properties.Settings.Default.PopSsl == true)
                checkBoxPopSsl.Checked = true;
            else
                checkBoxPopSsl.Checked = false;

            // Encryption settings
            textBoxPassphrase1.Text = Properties.Settings.Default.EncryptionPassphrase;
            textBoxPassphrase2.Text = Properties.Settings.Default.EncryptionPassphrase;

            // Button color
            buttonSettingsSave.BackColor = Properties.Settings.Default.BackgroundColor;
            buttonSettingsCencel.BackColor = Properties.Settings.Default.BackgroundColor;
            Color defaultColor = new Color();
            defaultColor = SystemColors.Control;
            if (Properties.Settings.Default.BackgroundColor != defaultColor)
                checkBoxBlue.Checked = true;
        }

        private void buttonSettingsSave_Click(object sender, EventArgs e)
        {   // Settings - Save
            
            // SMTP settings
            string SmtpServer = textBoxSmtpServer.Text;
            string SmtpPort = textBoxSmtpPort.Text;
            string SmtpUser = textBoxSmtpUsername.Text;
            string SmtpPass1 = textBoxSmtpPassword1.Text;
            string SmtpPass2 = textBoxSmtpPassword2.Text;
            bool SmtpSsl = false;
            if (checkBoxSmtpSsl.Checked == true)
                SmtpSsl = true;

            // POP settings
            string PopServer = textBoxPopServer.Text;
            string PopPort = textBoxPopPort.Text;
            string PopUser = textBoxPopUsername.Text;
            string PopPass1 = textBoxPopPassword1.Text;
            string PopPass2 = textBoxPopPassword2.Text;
            bool PopSsl = false;
            if (checkBoxPopSsl.Checked == true)
                PopSsl = true;
            
            // Encryption settings
            string EncrPass1 = textBoxPassphrase1.Text;
            string EncrPass2 = textBoxPassphrase2.Text;

            if (SmtpPass1 != SmtpPass2)
                MessageBox.Show("SMTP passwords are NOT identical!");
            else if (PopPass1 != PopPass2)
                MessageBox.Show("POP passwords are NOT identical!");
            else if (EncrPass1 != EncrPass2)
                MessageBox.Show("Encryption passwords are NOT identical!");
            else
            {
                // Set new values for SMTP
                Properties.Settings.Default.SmtpServer = SmtpServer;
                Properties.Settings.Default.SmtpPort = Convert.ToInt16(SmtpPort);
                Properties.Settings.Default.SmtpUsername = SmtpUser;
                Properties.Settings.Default.SmtpPassword = SmtpPass1;
                Properties.Settings.Default.SmtpSsl = SmtpSsl;

                // Set new values for POP
                Properties.Settings.Default.PopServer = PopServer;
                Properties.Settings.Default.PopPort = Convert.ToInt16(PopPort);
                Properties.Settings.Default.PopUsername = PopUser;
                Properties.Settings.Default.PopPassword = PopPass1;
                Properties.Settings.Default.PopSsl = PopSsl;

                // Set new value for Encryption
                Properties.Settings.Default.EncryptionPassphrase = EncrPass1;

                // Save, and close
                Properties.Settings.Default.Save();
                this.Hide();
            }
        }

        private void buttonSettingsCencel_Click(object sender, EventArgs e)
        {   // Settings - Cancel
            //MessageBox.Show("Nothing will be saved...");
            this.Hide();
        }

        private void textBoxSmtpServer_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxBlue_CheckedChanged(object sender, EventArgs e)
        {
            Color newColor = new Color();

            if (checkBoxBlue.Checked)
                newColor = System.Drawing.Color.LightBlue;
            else
                newColor = SystemColors.Control;

            // Change color instantly
            buttonSettingsSave.BackColor = newColor;
            buttonSettingsCencel.BackColor = newColor;
            this.Refresh();

            // Save changes to settings
            Properties.Settings.Default.BackgroundColor = newColor;
            Properties.Settings.Default.Save();
        }
    }
}
